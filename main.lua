-- Importing Libraries
require("libs/Lovetoys/src/engine")
flux = require "libs/flux/flux"

-- Imporitng Core
require("src/core/resources")
require("src/core/stackhelper")
require("src/core/state")

-- States
require("src/states/MenuState")
require("src/states/GameState")

-- Utils
require("src/utils/saves")
require("src/utils/tables")
require("data/settings")

function love.load()
   resources = Resources()

   -- Load Image 
   resources:addImage("ball", "res/img/hampster.png")
   resources:addImage("handle", "res/img/handle.png")

   -- Load Sound
   resources:addSound("ballHit", "res/sound/pong.wav", "static")

   -- Load Fonts
   resources:addFont("arial_48", "res/fonts/arial.ttf", 48)
   resources:addFont("arial_32", "res/fonts/arial.ttf", 32)

   resources:load()

   -- Load Settings
   set = Settings()
   Saves():loadSettings()

   -- Loading Stack
   stack = StackHelper()
   stack:push(MenuState())
end

function love.update(dt)
   stack:update(dt)
end

function love.draw()
   stack:draw()
end

function love.keypressed(key, isRepeat)
   stack:current():keypressed(key, isRepeat)
end

function love.mousepressed(x, y, button)
   stack:current():mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
   stack:current():mousereleased(x, y, button)
end

function love.quit()
   -- Add save stuff
end
