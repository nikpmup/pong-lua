PlayerTwoSys = class("PlayerTwoSys", System)

function PlayerTwoSys:__init(speed)
   self.speed = speed
end

function PlayerTwoSys:update(dt)
   move = 0
   if love.keyboard.isDown("i") then
      move = -1
   elseif love.keyboard.isDown("k") then
      move = 1
   end

   for i, entity in pairs(self.targets) do
      local physics = entity:get("PhysicsCmp")
      local body = physics.fixture:getBody()

      body:setLinearVelocity(0, move * self.speed)
   end
end

function PlayerTwoSys:requires()
   return {"PhysicsCmp", "PlayerTwoCmp"}
end
