PlayerOneSys = class("PlayerOneSys", System)

function PlayerOneSys:__init(speed)
   self.speed = speed
end

function PlayerOneSys:update(dt)
   move = 0
   if love.keyboard.isDown("w") then
      move = -1
   elseif love.keyboard.isDown("s") then
      move = 1
   end

   for i, entity in pairs(self.targets) do
      local physics = entity:get("PhysicsCmp")
      local body = physics.fixture:getBody()

      body:setLinearVelocity(0, move * self.speed)
   end
end

function PlayerOneSys:requires()
   return {"PhysicsCmp", "PlayerCmp"}
end
