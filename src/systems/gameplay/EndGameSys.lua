EndGameSys = class("EndGameSys", System)

function EndGameSys:__init(radius)
   self.radius = radius
end

function EndGameSys:update(dt)
   for i, entity in pairs(self.targets) do
      local position = entity:get("PositionCmp")

      if position.x < -self.radius or 
         position.x > love.window.getWidth() + self.radius then
         love.event.quit()
      end
   end
end

function EndGameSys:requires()
   return { "PositionCmp", "IsBallCmp" }
end
