MenuSys = class("MenuSys")

function MenuSys:__init(selColor, defColor)
   self.selColor = selColor
   self.defColor = defColor
   self.menuItems = {}
   self.funcs = {}
   self.curSel = 1
end

function MenuSys:fireEvent(event)
   self:resetSel()
   if event.key == "up" or event.key == "w" then
      self.curSel = self.curSel == 1 and #self.menuItems  or self.curSel - 1
   elseif event.key == "down" or event.key == "s" then
      self.curSel = self.curSel == #self.menuItems and 1 or self.curSel + 1 
   elseif event.key == "left" or event.key == "a" then
      self.funcs[self.curSel]("left")
   elseif event.key == "right" or event.key == "d" then
      self.funcs[self.curSel]("right")
   elseif event.key == "return" then
      self.funcs[self.curSel]("enter")
   end

   self:setSel(self.curSel)
end

function MenuSys:resetSel()
   local entity = self.menuItems[self.curSel]
   local text = entity:get("TextCmp")

   text.color = self.defColor
end

function MenuSys:setSel(idx)
   local entity = self.menuItems[idx]
   local text = entity:get("TextCmp")

   text.color = self.selColor
end

function MenuSys:addItem(menuEntity, func)
   local idx = #self.menuItems + 1

   self.menuItems[idx] = menuEntity 
   self.funcs[idx] = func
end
