SoundSys = class("SoundSys")

function SoundSys:__init()
   self.component1 = "SoundCmp"
   self.component2 = "Everything"
end

function SoundSys:action(entities)
   local sound = entities.entity1:get("SoundCmp")

   sound.source:play()
end
