RenderImageSys = class("RenderImageSys", System)

function RenderImageSys:draw()
   for i, v in pairs(self.targets) do
      local position = v:get("PositionCmp")
      local image = v:get("ImageCmp")
      local physics = v:get("PhysicsCmp")
      local scale = v:get("ScaleCmp")
      local body = physics.fixture:getBody()

      love.graphics.draw(image.img, position.x, position.y,
            body:getAngle(), scale.x, scale.y,
            image.img:getWidth() / 2, image.img:getHeight() / 2)
   end
end

function RenderImageSys:requires()
   return { "PositionCmp", "ImageCmp", "PhysicsCmp", "ScaleCmp" }
end
