MenuCreator = class("MenuCreator")

function MenuCreator:createMenuItem(menuSys, func, string, x, y, val)
   return self:createItem(menuSys, func, string, x, y, menuSys.defColor, val)
end

function MenuCreator:createMenuItemSel(menuSys, func, string, x, y, val)
   return self:createItem(menuSys, func, string, x, y, menuSys.selColor, val)
end

function MenuCreator:createItem(menuSys, func, string, x, y, color, val)
   local entity = Entity()

   entity:add(PositionCmp(x, y))
   entity:add(TextCmp(resources.fonts.arial_32, color, string, val))
   entity:add(AlignCmp(love.window.getWidth(), "left"))

   menuSys:addItem(entity, func)

   return entity
end

function MenuCreator:createTitle(string, x, y)
   local entity = Entity()

   entity:add(PositionCmp(x, y))
   entity:add(TextCmp(resources.fonts.arial_48, { 255, 255, 255, 255 }, string))
   entity:add(AlignCmp(0, "center"))

   return entity
end
