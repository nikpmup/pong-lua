GameCreator = class("GameCreator")

-- Create entity using an image and shape of image for physics
function GameCreator:createBoundedImage(img, x, y, world, phyType)
   local entity = Entity()

   -- Physics Values
   local body = love.physics.newBody(world, x, y, phyType)
   local shape = love.physics.newRectangleShape(img:getWidth(), img:getHeight())
   local fixture = love.physics.newFixture(body, shape, 1)

   -- Stores the entity to the fixture
   fixture:setUserData(entity)

   -- Adds components to entity
   entity:add(PositionCmp(x, y))
   entity:add(ImageCmp(img))
   entity:add(PhysicsCmp(body, shape, fixture))
   entity:add(ScaleCmp(1, 1))

   return entity
end

-- Ensures that image is created within the screen space and image dimensions
-- is the physics shape
function GameCreator:createBoundedImageInScreen(image, x, y, world, phyType)
   local imgX = image:getWidth()
   local imgY = image:getHeight()

   function bounds(pos, img, edge)
      if pos + img > edge then
         return edge - img / 2
      elseif pos - img < 0 then
         return img / 2
      else
         return pos
      end
   end

   local realX = bounds(x, imgX, love.graphics.getWidth())
   local realY = bounds(y, imgY, love.graphics.getHeight())

   return self:createBoundedImage(image, realX, realY, world, phyType) 
end
