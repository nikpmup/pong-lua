-- Importing Core
require("src/core/resources")
require("src/core/state")

-- Systems
require("src/systems/effects/CollScaleSys")
require("src/systems/events/GameKeySys")
require("src/systems/gameplay/EndGameSys")
require("src/systems/physics/PhysicsSys")
require("src/systems/physics/VelocityLimiterSys")
require("src/systems/player/PlayerOneSys")
require("src/systems/player/PlayerTwoSys")
require("src/systems/renderers/RenderImageSys")
require("src/systems/renderers/RenderTextSys")
require("src/systems/sounds/SoundSys")

-- Components
require("src/components/effects/ScaleEffectCmp")
require("src/components/identity/IsBallCmp")
require("src/components/physics/PhysicsCmp")
require("src/components/physics/PositionCmp")
require("src/components/physics/VelocityLimiterCmp")
require("src/components/player/PlayerCmp")
require("src/components/player/PlayerTwoCmp")
require("src/components/renderers/AlignCmp")
require("src/components/renderers/ImageCmp")
require("src/components/renderers/ScaleCmp")
require("src/components/renderers/TextCmp")
require("src/components/sounds/SoundCmp")

-- Events
require("src/events/physics/BeginContact")
require("src/events/inputs/keyPressed")

-- Utilities
require("src/creators/GameCreator")

-- States
require("src/states/PauseState")

GameState = class("GameState", State)

function GameState:load()
   -- Local variables
   world = love.physics.newWorld(0, 0, true) 
   world:setCallbacks(beginContact)

   -- Love toys
   self.engine = Engine()
   self.eventManager = EventManager()
   self.collManager = CollisionManager()

   -- Draw Systems
   self.engine:addSystem(RenderImageSys(), "draw", 10)
   self.engine:addSystem(RenderTextSys(), "draw", 11)

   -- Logic Systems
   self.engine:addSystem(PhysicsSys(0, 0, true), "logic", 1)
   self.engine:addSystem(PlayerOneSys(1200), "logic", 2)
   self.engine:addSystem(PlayerTwoSys(1200), "logic", 2)
   self.engine:addSystem(VelocityLimiterSys(), "logic", 3)
   self.engine:addSystem(EndGameSys(42), "logic", 4)

   -- Event Handling
   self.gameKeySys = GameKeySys()
   self.eventManager:addListener("BeginContact", {self.collManager, self.collManager.fireEvent})
   self.eventManager:addListener("KeyPressed", {self.gameKeySys, self.gameKeySys.fireEvent})

   -- Collision Manager
   local soundSys = SoundSys()
   self.collManager:addCollisionAction(soundSys.component1, soundSys.component2, soundSys)
   local collScaleSys = CollScaleSys()
   self.collManager:addCollisionAction(collScaleSys.component1, collScaleSys.component2, collScaleSys)

   -- Entity Creation
   self:createHampster()
   self:createPlayers()
   self:createBounds()
   self:createText()
end

function GameState:draw()
   self.engine:draw()
   love.graphics.setColor(255, 255, 255, 255)
end

-- Frame Refresh
function GameState:update(dt)
   flux.update(dt)
   world:update(dt)
   self.engine:update(dt)
end

function GameState:keypressed(key, isRepeat)
   self.eventManager:fireEvent(KeyPressed(key, isRepeat))
end

function GameState:mousepressed(x, y, button)
end

function GameState:mousereleased(x, y, button)
end

function beginContact(fixtureA, fixtureB, contact)
   stack:current().eventManager:fireEvent(BeginContact(fixtureA, fixtureB, contact))
end

function GameState:createPlayers()
   player = GameCreator:createBoundedImageInScreen(resources.images.handle,
         0, love.window.getWidth()/2, world,
         "kinematic")
   player:add(PlayerCmp())
   player:add(ScaleEffectCmp(1000, 1000, 1200, 1200, 25, 50))
   player:add(SoundCmp(resources.sounds.ballHit))
   self.engine:addEntity(player)

   playerTwo = GameCreator:createBoundedImageInScreen(resources.images.handle, 
         love.window.getWidth(), love.window.getWidth()/2, 
         world, "kinematic")
   playerTwo:add(PlayerTwoCmp())
   playerTwo:add(ScaleEffectCmp(1000, 1000, 1200, 1200, 25, 50))
   playerTwo:add(SoundCmp(resources.sounds.ballHit))
   self.engine:addEntity(playerTwo)
end

function GameState:createHampster()
   local hampster = Entity()

   local image = ImageCmp(resources.images.ball)
   local position = PositionCmp(love.window.getWidth() / 2, love.window.getHeight() / 2)

   local body = love.physics.newBody(world, position.x, position.y, "dynamic")
   local shape = love.physics.newCircleShape(image.img:getWidth() / 3)
   local fixture = love.physics.newFixture(body, shape, 1)
   fixture:setRestitution(1.1)
   fixture:setUserData(hampster)
   body:setLinearVelocity(-300, 0)
   local physics = PhysicsCmp(body, shape, fixture)

   hampster:add(position)
   hampster:add(image)
   hampster:add(physics)
   hampster:add(ScaleCmp(1, 1))
   hampster:add(VelocityLimiterCmp(300, 0))
   hampster:add(IsBallCmp())
   hampster:add(ScaleEffectCmp(900, 1200, 1000, 1400, 100, 200))

   self.engine:addEntity(hampster)
end

function GameState:createBounds()
   local topWall = Entity()
   local body = love.physics.newBody(world, 0, 0, "static")
   local shape = love.physics.newChainShape(false, 0, 0, love.window.getWidth(), 0)
   local fixture = love.physics.newFixture(body, shape, 1)
   fixture:setUserData(topWall)
   topWall:add(ScaleCmp(1, 1))
   topWall:add(SoundCmp(resources.sounds.ballHit))

   local bottomWall = Entity()
   local body2 = love.physics.newBody(world, 0, 0, "static")
   local shape2 = love.physics.newChainShape(false, 0, love.window.getHeight(), 
         love.window.getWidth(), love.window.getHeight())
   local fixture = love.physics.newFixture(body2, shape2, 1)
   fixture:setUserData(topWall)
   bottomWall:add(ScaleCmp(1, 1))
   bottomWall:add(SoundCmp(resources.sounds.ballHit))
end

function GameState:createText()
   local title = Entity()
   
   title:add(PositionCmp(love.window.getWidth() / 2, 0))
   title:add(TextCmp(resources.fonts.arial_48, { 255, 255, 255, 255 }, "Pong"))
   title:add(AlignCmp(0, "center"))

   self.engine:addEntity(title)
end
