-- Core Libraries
require("src/core/resources")
require("src/core/state")
require("src/utils/saves")
require("settings")

-- Systems
require("src/systems/renderers/RenderTextSys")
require("src/systems/events/MenuSys")

-- Components
require("src/components/physics/PositionCmp")
require("src/components/renderers/AlignCmp")
require("src/components/renderers/TextCmp")

-- Events
require("src/events/inputs/keyPressed")

-- Creators
require("src/creators/MenuCreator")

SettingsState = class("SettingsState", States)

function SettingsState:load()
   -- Lovetoys
   self.engine = Engine()
   self.eventManager = EventManager()

   -- Event Systems
   self.menuSys = MenuSys({ 255, 128, 0, 255 }, { 255, 255, 255, 255})

   -- Event Handling
   self.eventManager:addListener("KeyPressed", { self.menuSys, self.menuSys.fireEvent })

   -- Draw Systems
   self.engine:addSystem(RenderTextSys(), "draw", 11)

   self:createSettingItems()
end

function SettingsState:update(dt)
   self.engine:update(dt)
end

function SettingsState:draw()
   self.engine:draw()

   love.graphics.setColor(255, 255, 255, 255)
end

function SettingsState:keypressed(key, isRepeat)
   self.eventManager:fireEvent(KeyPressed(key, isRepeat))
end

function SettingsState:mousepressed(x, y, button)
end

function SettingsState:mousereleased(x, y, button)
end

function SettingsState:createSettingItems()
   local posY = 0
   local posX = 26
   local title = MenuCreator:createTitle("Settings", 
   love.window.getWidth() / 2, posY)
   posY = posY + 48
   local resolution = MenuCreator:createMenuItemSel(self.menuSys, changeResolution, 
         "Resolution: %sx%s", posX, posY, {{set.settings, "resolution"}})
   posY = posY + 32
   local fullScreen = MenuCreator:createMenuItem(self.menuSys, changeFullscreen,
         "Fullscreen: %s", posX, posY, {{set.settings, "fullscreen"}})
   posY = posY + 32
   local audio = MenuCreator:createMenuItem(self.menuSys, changeAudio,
         "Audio: %s", posX, posY, {{set.settings, "audio"}})
   posY = posY + 64
   local save  = MenuCreator:createMenuItem(self.menuSys, saveFunc, 
         "Save", posX, posY)   
   local back = MenuCreator:createMenuItem(self.menuSys, backFunc, 
         "Back", posX + 100, posY)   

   self.engine:addEntity(title)
   self.engine:addEntity(resolution)
   self.engine:addEntity(fullScreen)
   self.engine:addEntity(audio)
   self.engine:addEntity(save)
   self.engine:addEntity(back)
end

function changeResolution(cmd)
   if (cmd == "right" and set.curRes ~= #set.resolutions) then
      set.curRes = set.curRes + 1
      set.settings.resolution = set.resolutions[set.curRes]
      Saves:refreshSettings()
      stack:current():load()
   elseif (cmd == "left" and set.curRes ~= 1) then
      set.curRes = set.curRes - 1
      set.settings.resolution = set.resolutions[set.curRes]
      Saves:refreshSettings()
      stack:current():load()
   end
end

function changeFullscreen(cmd)
   if (cmd == "left" or cmd == "right") then
      set.settings.fullscreen = not set.settings.fullscreen
      Saves:refreshSettings()
      stack:current():load()
   end
end

function changeAudio(cmd)
   if (cmd == "left" and set.settings.audio ~= 0) then
      set.settings.audio = set.settings.audio - 1
   elseif (cmd == "right" and set.settings.audio ~= 100) then
      set.settings.audio = set.settings.audio + 1
   end
end

function backFunc(cmd)
   if (cmd == "enter") then
      set = Settings()
      Saves:loadSettings()
      stack:pop()
   elseif (cmd == "left") then
      local stackMenuSys = stack:current().menuSys
      stackMenuSys.curSel = stackMenuSys.curSel - 1
   end
end

function saveFunc(cmd)
   if (cmd == "enter") then
      Saves:refreshSettings()
      Saves:saveSettings()
      stack:popload()
   elseif (cmd == "right") then
      local stackMenuSys = stack:current().menuSys
      stackMenuSys.curSel = stackMenuSys.curSel + 1
   end
end
