-- Core Libraries
require("src/core/resources")
require("src/core/state")

-- Systems
require("src/systems/renderers/RenderTextSys")
require("src/systems/events/MenuSys")

-- Components
require("src/components/physics/PositionCmp")
require("src/components/renderers/AlignCmp")
require("src/components/renderers/TextCmp")

-- Events
require("src/events/inputs/keyPressed")
require("src/events/inputs/mousePressed")

-- Creators
require("src/creators/MenuCreator")

-- States
require("src/states/GameState")
require("src/states/SettingsState")

MenuState = class("MenuState", State)

function MenuState:load()
   -- Load LoveToys
   self.engine = Engine()
   self.eventManager = EventManager()

   -- Event Systems
   self.menuSys = MenuSys({ 255, 128, 0, 255 }, { 255, 255, 255, 255 })

   -- Event Handling
   self.eventManager:addListener("KeyPressed", { self.menuSys, self.menuSys.fireEvent })

   -- Draw Systems
   self.engine:addSystem(RenderTextSys(), "draw", 11)

   self:createMenuItems()
end

function MenuState:update(dt)
   self.engine:update(dt)
end

function MenuState:draw()
   self.engine:draw()
   love.graphics.setColor(255, 255, 255, 255)
end

function MenuState:keypressed(key, isRepeat)
   self.eventManager:fireEvent(KeyPressed(key, isRepeat))
end

function MenuState:mousepressed(x, y, button)
end

function MenuState:mousereleased(x, y, button)
end

function MenuState:createMenuItems()
   local bottom = love.graphics.getHeight() - 48

   local title = MenuCreator:createTitle("Pong",
         love.graphics.getWidth() / 2, 0)
   local play = MenuCreator:createMenuItemSel(self.menuSys, enterGame, "Play", 25, bottom - 64)   
   local settings = MenuCreator:createMenuItem(self.menuSys, enterSettings, "Settings", 26, bottom - 32)   
   local quit = MenuCreator:createMenuItem(self.menuSys, exitGame, "Quit", 25, bottom)   

   self.engine:addEntity(title)
   self.engine:addEntity(play)
   self.engine:addEntity(settings)
   self.engine:addEntity(quit)
end

function enterGame(cmd)
   if (cmd == "enter") then
      stack:push(GameState())
   end
end

function enterSettings(cmd)
   if (cmd == "enter") then
      stack:push(SettingsState())
   end
end

function exitGame(cmd)
   if (cmd == "enter") then
      love.event.quit()
   end
end
